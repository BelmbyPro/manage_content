package org.acfse.adapters;

import lombok.AllArgsConstructor;
import org.acfse.data.HashTagDto;
import org.acfse.data.models.HashTagModel;
import org.acfse.ports.api.HashTagServicePort;
import org.acfse.ports.spi.HashTagPersistencePort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class HashTagServiceAdapter implements HashTagServicePort {
    @Autowired
    private HashTagPersistencePort hashTagPersistencePort;

    @Override
    public void addHashTag(HashTagDto hashTagDto) {

        hashTagPersistencePort.addHashTag(hashTagDto);
    }

    @Override
    public List<HashTagModel> getAllHashTags() {

        return hashTagPersistencePort.getAllHashTags();
    }

    @Override
    public HashTagModel getHashTagByName(String name) {

        return hashTagPersistencePort.getHashTagByName(name);
    }

    @Override
    public HashTagModel getHashTagById(Long id) {
        return hashTagPersistencePort.getHashTagById(id);
    }
}
