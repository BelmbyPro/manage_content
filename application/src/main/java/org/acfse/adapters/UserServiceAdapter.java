package org.acfse.adapters;

import lombok.AllArgsConstructor;
import org.acfse.data.UserDto;
import org.acfse.data.models.UserModel;
import org.acfse.ports.api.UserServicePort;
import org.acfse.ports.spi.UserPersistencePort;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class UserServiceAdapter implements UserServicePort {

    private UserPersistencePort userPersistencePort;

    @Override
    public void addUser(UserDto userDto) {

         userPersistencePort.addUser(userDto);
    }

    @Override
    public void deleteUserById(Long id) {

        userPersistencePort.deleteUserById(id);
    }

    @Override
    public void updateUser(UserModel userModel) {

         userPersistencePort.updateUser(userModel);
    }

    @Override
    public List<UserModel> getUsers() {

        return userPersistencePort.getUsers();
    }

    @Override
    public UserModel getUserById(Long id) {

        return userPersistencePort.getUserById(id);
    }

    @Override
    public UserModel getUserByEmail(String email) {

        return userPersistencePort.getUserByEmail(email);
    }
}
