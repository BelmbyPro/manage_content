package org.acfse.adapters;

import lombok.AllArgsConstructor;
import org.acfse.data.CategoryDto;
import org.acfse.data.models.CategoryModel;
import org.acfse.ports.api.CategoryServicePort;
import org.acfse.ports.spi.CategoryPersistencePort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class CategoryServiceAdapter implements CategoryServicePort {

    private CategoryPersistencePort categoryPersistencePort;

    @Override
    public void addCategory(CategoryDto categoryDto) {
        categoryPersistencePort.addCategory(categoryDto);
    }

    @Override
    public List<CategoryModel> getAllCategories() {
        return categoryPersistencePort.getAllCategories();
    }

    @Override
    public CategoryModel getCategoryByName(String name) {
        return categoryPersistencePort.getCategoryByName(name);
    }

    @Override
    public CategoryModel getCategoryById(Long id) {
        return categoryPersistencePort.getCategoryById(id);
    }
}
