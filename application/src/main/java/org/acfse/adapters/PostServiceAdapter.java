package org.acfse.adapters;

import lombok.AllArgsConstructor;
import org.acfse.data.PostDto;
import org.acfse.data.models.PostModel;
import org.acfse.ports.api.PostServicePort;
import org.acfse.ports.spi.PostPersistencePort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class PostServiceAdapter implements PostServicePort {


    PostPersistencePort postPersistencePort;

    @Override
    public List<PostModel> getAllPosts() {
        return postPersistencePort.getAllPosts();
    }

    @Override
    public void addPost(PostDto postDto) {

        postPersistencePort.addPost(postDto);
    }

    @Override
    public List<PostModel> getAllQuestions() {
        return postPersistencePort.getAllQuestions();
    }

    @Override
    public List<PostModel> getAllStories() {
        return postPersistencePort.getAllStories();
    }
}
