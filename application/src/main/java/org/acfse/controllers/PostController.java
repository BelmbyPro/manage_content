package org.acfse.controllers;

import lombok.AllArgsConstructor;
import org.acfse.data.PostDto;
import org.acfse.data.models.PostModel;
import org.acfse.exceptions.BadRequestException;
import org.acfse.exceptions.ResourceNotFoundException;
import org.acfse.ports.api.CategoryServicePort;
import org.acfse.ports.api.HashTagServicePort;
import org.acfse.ports.api.PostServicePort;
import org.acfse.ports.api.UserServicePort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/rest/post")
@AllArgsConstructor
public class PostController {
    private UserServicePort userServicePort;
    private CategoryServicePort categoryServicePort;
    private HashTagServicePort hashTagServicePort;
    private PostServicePort postServicePort;

    @PostMapping
    ResponseEntity addPost(@RequestBody @Valid PostDto postDto) {

        if (userServicePort.getUserById(postDto.getUserId()) == null) {
            throw new BadRequestException("user with ID : " + postDto.getUserId() + " Not exist");
        }

        if (categoryServicePort.getCategoryById(postDto.getCategoryId()) == null) {
            throw new BadRequestException("Category with ID : " + postDto.getCategoryId() + " Not exist");
        }

        if (postDto.getHashTagIds() != null && !postDto.getHashTagIds().isEmpty()) {
            postDto.getHashTagIds().forEach(id -> {
                if (hashTagServicePort.getHashTagById(id) == null) {
                    throw new BadRequestException("HashTag with ID : " + id + " Not exist");
                }
            });

        }

        postServicePort.addPost(postDto);

        return new ResponseEntity(HttpStatus.CREATED);
    }

    @GetMapping("/posts")
    ResponseEntity<List<PostModel>> getAllPosts() {
        List<PostModel> postModels = postServicePort.getAllPosts();
        return getListResponseEntity(postModels,"posts not found");
    }

    @GetMapping("/questions")
    ResponseEntity<List<PostModel>> getAllQuestions() {
        List<PostModel> postModels = postServicePort.getAllQuestions();
        return getListResponseEntity(postModels,"questions not found");
    }

    @GetMapping("/stories")
    ResponseEntity<List<PostModel>> getAllStories() {
        List<PostModel> postModels = postServicePort.getAllStories();
        return getListResponseEntity(postModels,"stories not found");
    }

    private ResponseEntity<List<PostModel>> getListResponseEntity(List<PostModel> postModels, String message) {
        if (postModels.isEmpty()) {
            throw new ResourceNotFoundException(message);
        }
        return new ResponseEntity<>(postModels, HttpStatus.OK);
    }

}
