package org.acfse.controllers;

import org.acfse.data.HashTagDto;
import org.acfse.data.models.HashTagModel;
import org.acfse.exceptions.BadRequestException;
import org.acfse.exceptions.ResourceNotFoundException;
import org.acfse.ports.api.HashTagServicePort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/rest/hashtag")
public class HashTagController {

    @Autowired
    HashTagServicePort hashTagServicePort;
    @PostMapping
    ResponseEntity addHashTag(@Valid @RequestBody HashTagDto hashTagDto){
        HashTagModel hashTagModel = hashTagServicePort.getHashTagByName(hashTagDto.getName());
        if (hashTagModel != null){
            throw new BadRequestException("This hashTag already exist");
        }
        hashTagServicePort.addHashTag(hashTagDto);

        return new ResponseEntity(HttpStatus.CREATED);
    }
    @GetMapping
    ResponseEntity<List<HashTagModel>> getAllHashTags(){
        List<HashTagModel> hashTagModels = hashTagServicePort.getAllHashTags();
        if(hashTagModels.isEmpty()) {
            throw new ResourceNotFoundException("hashTags not found");
        }
        return new ResponseEntity<>(hashTagModels,HttpStatus.OK);
    }
}
