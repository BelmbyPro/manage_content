package org.acfse.controllers;

import org.acfse.data.CategoryDto;
import org.acfse.data.models.CategoryModel;
import org.acfse.exceptions.BadRequestException;
import org.acfse.exceptions.ResourceNotFoundException;
import org.acfse.ports.api.CategoryServicePort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/rest/category")
public class CategoryController {
    @Autowired
    CategoryServicePort categoryServicePort;

    @PostMapping
    ResponseEntity addCategory(@Valid @RequestBody CategoryDto categoryDto) {

        CategoryModel categoryModel = categoryServicePort.getCategoryByName(categoryDto.getName());
        if (categoryModel != null) {
            throw new BadRequestException("This category already exist");
        }
        categoryServicePort.addCategory(categoryDto);

        return new ResponseEntity(HttpStatus.CREATED);
    }

    @GetMapping
    ResponseEntity<List<CategoryModel>> getAllCategories() {
        List<CategoryModel> categoryModels = categoryServicePort.getAllCategories();
        if (categoryModels.isEmpty()) {
            throw new ResourceNotFoundException("categories not found");
        }
        return new ResponseEntity<>(categoryModels, HttpStatus.OK);
    }
}
