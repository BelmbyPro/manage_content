package org.acfse.controllers;

import org.acfse.data.UserDto;
import org.acfse.data.models.UserModel;
import org.acfse.exceptions.BadRequestException;
import org.acfse.exceptions.ResourceNotFoundException;
import org.acfse.ports.api.UserServicePort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/rest/user")
public class UserController {
    @Autowired
    private UserServicePort userServicePort;

    @PostMapping
    ResponseEntity addUser(@Valid @RequestBody UserDto userDto) {

        UserModel userModel  = userServicePort.getUserByEmail(userDto.getEmail());

        if (userModel != null){
            throw new BadRequestException("This User Already exist");
            // 409 http conflict
        }
        userServicePort.addUser(userDto);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @DeleteMapping("/{user-ID}")
    ResponseEntity deleteUserById(@PathVariable(name = "user-ID") Long id) {
        UserModel  userModel  = userServicePort.getUserById(id);
        if (userModel == null){
            throw new ResourceNotFoundException("The user you are trying to delete does not exist");
        }
        userServicePort.deleteUserById(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @PutMapping
    ResponseEntity updateUser(@RequestBody UserModel userModel) {
        UserModel model  = userServicePort.getUserById(userModel.getId());
        if (model == null){
            throw new ResourceNotFoundException("The user you are trying to update does not exist");
        }
        userServicePort.updateUser(userModel);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @GetMapping("/users")
    ResponseEntity<List<UserModel>> getUsers() {
        List<UserModel> userModels = userServicePort.getUsers();
        if (userModels.isEmpty()) {
            throw new ResourceNotFoundException("users Not Found");
        }
        return new ResponseEntity<>(userModels, HttpStatus.OK);
    }

    @GetMapping("/{user-ID}")
    ResponseEntity<UserModel> getUserById(@PathVariable(name = "user-ID") Long id) {

        UserModel userModel  = userServicePort.getUserById(id);
        if (userModel == null) {
            throw new ResourceNotFoundException("user with ID : " + id + " Not Found");
        }
        return new ResponseEntity(userModel, HttpStatus.OK);
    }
}
