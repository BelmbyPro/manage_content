package org.acfse.repositories;

import org.acfse.entities.HashTagEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface HashTagRepository extends JpaRepository<HashTagEntity,Long> {
    Optional<HashTagEntity> findByName(String name);
}
