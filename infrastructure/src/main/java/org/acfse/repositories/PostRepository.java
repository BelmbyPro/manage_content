package org.acfse.repositories;

import org.acfse.entities.PostEntity;
import org.acfse.entities.QuestionEntity;
import org.acfse.entities.StoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostRepository<T extends PostEntity> extends JpaRepository<T,Long> {
    @Query("from QuestionEntity")
    List<QuestionEntity> findAllQuestionEntities();
    @Query("from StoryEntity")
    List<StoryEntity> findAllStoryEntities();
}
