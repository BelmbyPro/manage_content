package org.acfse.repositories;

import org.acfse.entities.ActionBetweenPostAndHashTagUniqueID;
import org.acfse.entities.ListeHashTagPost;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ListeHashTagPostRepository extends JpaRepository<ListeHashTagPost, ActionBetweenPostAndHashTagUniqueID> {
}
