package org.acfse;

import org.acfse.data.CategoryDto;
import org.acfse.data.HashTagDto;
import org.acfse.data.PostDto;
import org.acfse.data.UserDto;
import org.acfse.data.enums.TypePost;
import org.acfse.ports.api.CategoryServicePort;
import org.acfse.ports.api.HashTagServicePort;
import org.acfse.ports.api.PostServicePort;
import org.acfse.ports.api.UserServicePort;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class AcfseApplication {
    public static void main(String[] args) {
        SpringApplication.run(AcfseApplication.class, args);
    }

    @Bean
    CommandLineRunner commandLineRunner(UserServicePort userServicePort, CategoryServicePort categoryServicePort, HashTagServicePort hashTagServicePort, PostServicePort postServicePort) {
        return args -> {
            userServicePort.addUser(UserDto.builder().nom("Belmondo").email("bel@gmail.com").build());
            userServicePort.addUser(UserDto.builder().nom("belmby").email("belmby@gmail.com").build());

            categoryServicePort.addCategory(CategoryDto.builder().name("cat1").build());
            categoryServicePort.addCategory(CategoryDto.builder().name("cat2").build());

            hashTagServicePort.addHashTag(HashTagDto.builder().name("tag1").build());
            hashTagServicePort.addHashTag(HashTagDto.builder().name("tag2").build());

            List<Long> tagIds = new ArrayList<Long>(Arrays.asList(1L,2L));
            postServicePort.addPost(PostDto.builder().categoryId(1L).userId(1L).type(TypePost.QUESTION).contentORfilename("l'argent qui es tu ?").title("question1").hashTagIds(tagIds).build());
            postServicePort.addPost(PostDto.builder().categoryId(1L).userId(1L).type(TypePost.STORY).contentORfilename("filename.txt").title("story1").hashTagIds(tagIds).build());
        };
    }
}
