package org.acfse.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "USERBLOQUER")
@IdClass(ActionBetweenUserAndOtherUserUniqueID.class)
public class UserBloquer extends AbstractClass {
    @Id
    @ManyToOne
    @JoinColumn(name = "USER_ID")
    private UserEntity userEntity;
    @Id
    @ManyToOne
    @JoinColumn(name = "USER_BLOQUER_ID")
    private UserEntity userSubject;
}
