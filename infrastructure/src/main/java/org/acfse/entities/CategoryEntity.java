package org.acfse.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Collection;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "CATEGORY")
public class CategoryEntity extends AbstractClass {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @OneToMany(mappedBy = "categoryEntity")
    private Collection<PostEntity> postEntities;
    @OneToMany(mappedBy = "categoryEntity")
    private Collection<CategorieSuivie> categorieSuivies;
}
