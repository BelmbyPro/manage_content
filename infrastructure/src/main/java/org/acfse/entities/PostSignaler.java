package org.acfse.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
@Table(name = "POSTSIGNALER")
@IdClass(ActionBetweenUserAndPostUniqueID.class)
public class PostSignaler extends AbstractClass {

    private String motif;
    @Id
    @ManyToOne
    @JoinColumn(name = "USER_ID")
    private UserEntity userEntity;
    @Id
    @ManyToOne
    @JoinColumn(name = "POST_ID")
    private PostEntity postEntity;

}
