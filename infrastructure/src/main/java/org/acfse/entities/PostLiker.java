package org.acfse.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
@Table(name = "POSTSLIKER")
@IdClass(ActionBetweenUserAndPostUniqueID.class)
public class PostLiker extends AbstractClass {
    @Id
    @ManyToOne
    @JoinColumn(name = "POST_ID")
    private PostEntity postEntity;
    @Id
    @ManyToOne
    @JoinColumn(name = "USER_ID")
    private UserEntity userEntity;
}
