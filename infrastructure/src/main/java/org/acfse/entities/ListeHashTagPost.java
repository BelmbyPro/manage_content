package org.acfse.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
@Table(name = "LISTEHASHTAGPOST")
//@IdClass(ActionBetweenPostAndHashTagUniqueID.class)
public class ListeHashTagPost extends AbstractClass {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //@Id
    @ManyToOne
    @JoinColumn(name = "POST_ID")
    private PostEntity postEntity;
    //@Id
    @ManyToOne
    @JoinColumn(name = "HASHTAG_ID")
    private HashTagEntity hashTagEntity;
}
