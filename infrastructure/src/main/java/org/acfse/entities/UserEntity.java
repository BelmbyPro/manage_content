package org.acfse.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.acfse.data.enums.Genre;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "USER")
public class UserEntity extends AbstractClass {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nom;
    private String prenom;
    private String email;
    @Temporal(TemporalType.DATE)
    private Date dateDeNaissance;
    private String descriptionDuProfil;
    @Enumerated(EnumType.STRING)
    private Genre genre;
    private String travail;
    private String ecole;
    private String adresse;
    private String langue;
    @OneToMany(mappedBy = "userEntity")
    private Collection<PostEntity> postEntities;
    @OneToMany(mappedBy = "userEntity")
    private Collection<PostSignaler> postSignalers;
    @OneToMany(mappedBy = "userEntity")
    private Collection<PostSauvegarder> postSauvegarders;
    @OneToMany(mappedBy = "userEntity")
    private Collection<PostLiker> postLikers;
    @OneToMany(mappedBy = "userEntity")
    private Collection<SuivreUser> suivreUsers;
    @OneToMany(mappedBy = "userSubject")
    private Collection<SuivreUser> usersuivie;
    @OneToMany(mappedBy = "userEntity")
    private Collection<UserBloquer> userBloqueur;
    @OneToMany(mappedBy = "userSubject")
    private Collection<UserBloquer> userBloquers;
    @OneToMany(mappedBy = "userEntity")
    private Collection<UserSignaler> userSignalers;
    @OneToMany(mappedBy = "userSubject")
    private Collection<UserSignaler> userSignaleur;
    @OneToMany(mappedBy = "userEntity")
    private Collection<CategorieSuivie> categorieSuivies;
}
