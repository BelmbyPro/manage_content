package org.acfse.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
@Table(name = "USERSIGNALER")
@IdClass(ActionBetweenUserAndOtherUserUniqueID.class)
public class UserSignaler extends AbstractClass {
    @Id
    @ManyToOne
    @JoinColumn(name = "USER_ID")
    private UserEntity userEntity;
    @Id
    @ManyToOne
    @JoinColumn(name = "USER_SIGNALER_ID")
    private UserEntity userSubject;

}
