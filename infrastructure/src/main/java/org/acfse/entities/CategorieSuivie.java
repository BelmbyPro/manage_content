package org.acfse.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "CATEGORIESUIVIE")
@IdClass(ActionBetweenUserAndCategorieUniqueID.class)
public class CategorieSuivie extends AbstractClass {

    @Id
    @ManyToOne
    private UserEntity userEntity;
    @Id
    @ManyToOne
    private CategoryEntity categoryEntity;
}
