package org.acfse.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@Builder
@Data @NoArgsConstructor @AllArgsConstructor
@DiscriminatorValue("QUESTION")
public class QuestionEntity extends PostEntity {
    private String content;
}
