package org.acfse.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Data
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "TYPE", length = 8)
@Table(name = "POST")
public abstract class PostEntity extends AbstractClass {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String title;
	@ManyToOne
	@JoinColumn(name = "USER_ID", nullable = false)
	private UserEntity userEntity;
	@ManyToOne
	@JoinColumn(name = "CATEGORY_ID", nullable = false)
	private CategoryEntity categoryEntity;
	@OneToMany(mappedBy = "postEntity")
	private Collection<PostSignaler> postSignalers;
	@OneToMany(mappedBy = "postEntity")
	private Collection<PostSauvegarder> postSauvegarders;
	@OneToMany(mappedBy = "postEntity")
	private Collection<PostLiker> postLikers;
	@OneToMany(mappedBy = "postEntity")
	private Collection<ListeHashTagPost> listeHashTagPosts;
}
