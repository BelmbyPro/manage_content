package org.acfse.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@Data @NoArgsConstructor @AllArgsConstructor
@DiscriminatorValue("STORY")
@Builder
public class StoryEntity extends PostEntity {
    private String filename;
}
