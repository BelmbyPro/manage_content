package org.acfse.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class ActionBetweenUserAndPostUniqueID implements Serializable {
    private static final long serialVersionUID = 2702030623316532366L;

    private UserEntity userEntity;
    private PostEntity postEntity;

}
