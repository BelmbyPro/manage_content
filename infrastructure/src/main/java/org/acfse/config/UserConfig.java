package org.acfse.config;

import org.acfse.adapters.UserPersistenceAdapter;
import org.acfse.adapters.UserServiceAdapter;
import org.acfse.ports.api.UserServicePort;
import org.acfse.ports.spi.UserPersistencePort;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UserConfig {

    @Bean
    public UserPersistencePort userPersistencePort() {
        return new UserPersistenceAdapter();
    }

    @Bean
    public UserServicePort userServicePort() {
        return new UserServiceAdapter(userPersistencePort());
    }
}
