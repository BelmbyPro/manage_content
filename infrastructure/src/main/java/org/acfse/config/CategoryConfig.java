package org.acfse.config;

import org.acfse.adapters.CategoryPersistenceAdapter;
import org.acfse.adapters.CategoryServiceAdapter;
import org.acfse.ports.api.CategoryServicePort;
import org.acfse.ports.spi.CategoryPersistencePort;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CategoryConfig {
    @Bean
    public CategoryPersistencePort categoryPersistencePort(){
        return new CategoryPersistenceAdapter();
    }

    @Bean
    public CategoryServicePort categoryServicePort(){
        return new CategoryServiceAdapter(categoryPersistencePort());
    }
}
