package org.acfse.config;

import org.acfse.adapters.HashTagPersistenceAdapter;
import org.acfse.adapters.HashTagServiceAdapter;
import org.acfse.ports.api.HashTagServicePort;
import org.acfse.ports.spi.HashTagPersistencePort;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HashTagConfig {
    @Bean
    public HashTagPersistencePort hashTagPersistencePort(){
        return new HashTagPersistenceAdapter();
    }

    @Bean
    public HashTagServicePort hashTagServicePort(){
        return  new HashTagServiceAdapter(hashTagPersistencePort());
    }
}
