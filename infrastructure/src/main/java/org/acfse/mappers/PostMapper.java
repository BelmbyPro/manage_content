package org.acfse.mappers;

import org.acfse.data.PostDto;
import org.acfse.data.enums.TypePost;
import org.acfse.data.models.PostModel;
import org.acfse.entities.*;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.ArrayList;
import java.util.List;
@Mapper(componentModel = "spring")
public abstract class PostMapper {

    //public abstract List<PostModel> listPostEntityToListPostModel(List<PostEntity> postEntities);

    @Mapping(target = "filename", ignore = true)
    public abstract List<PostModel> listQuestionEntityToListPostModel(List<QuestionEntity> questionEntities);

    @Mapping(target = "content", ignore = true)
    public abstract List<PostModel> listStoryEntityToListPostModel(List<StoryEntity> storyEntities);

    public StoryEntity postDtoToStoryEntity(PostDto postDto, UserEntity userEntity, CategoryEntity categoryEntity) {
        StoryEntity storyEntity = new StoryEntity();
        storyEntity.setTitle(postDto.getTitle());
        storyEntity.setFilename(postDto.getContentORfilename());
        storyEntity.setUserEntity(userEntity);
        storyEntity.setCategoryEntity(categoryEntity);
        return storyEntity;
    }

    public QuestionEntity postDtoToQuestionEntity(PostDto postDto, UserEntity userEntity, CategoryEntity categoryEntity) {
        QuestionEntity questionEntity = new QuestionEntity();
        questionEntity.setTitle(postDto.getTitle());
        questionEntity.setContent(postDto.getContentORfilename());
        questionEntity.setUserEntity(userEntity);
        questionEntity.setCategoryEntity(categoryEntity);
        return questionEntity;
    }

    public List<PostModel> listPostEntityToListPostModel(List<PostEntity> postEntities) {
        if (postEntities == null) {
            return null;
        }

        List<PostModel> list = new ArrayList<PostModel>(postEntities.size());
        for (PostEntity postEntity : postEntities) {
            list.add(postEntityToPostModel(postEntity));
        }

        return list;
    }

    private PostModel postEntityToPostModel(PostEntity postEntity) {
        if (postEntity == null) {
            return null;
        }

        PostModel.PostModelBuilder postModel = PostModel.builder();

        postModel.id(postEntity.getId());
        postModel.title(postEntity.getTitle());
        if (postEntity instanceof QuestionEntity) {
            postModel.content(((QuestionEntity) postEntity).getContent());
            postModel.type(TypePost.QUESTION);
        }
        if (postEntity instanceof StoryEntity) {
            postModel.filename(((StoryEntity) postEntity).getFilename());
            postModel.type(TypePost.STORY);
        }
        return postModel.build();
    }

}
