package org.acfse.mappers;

import org.acfse.data.CategoryDto;
import org.acfse.data.models.CategoryModel;
import org.acfse.entities.CategoryEntity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CategoryMapper {
    CategoryMapper INSTANCE = Mappers.getMapper(CategoryMapper.class);

    CategoryEntity categoryDtoToCategoryEntity(CategoryDto categoryDto);

    List<CategoryModel> listCategoryEntityToListCategoryModel(List<CategoryEntity> categoryEntities);

    CategoryModel categoryEntityToCategoryModel(CategoryEntity categoryEntity);

    CategoryEntity categoryModelToCategoryEntity(CategoryModel categoryModel);
}
