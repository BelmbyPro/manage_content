package org.acfse.mappers;

import org.acfse.data.HashTagDto;
import org.acfse.data.models.HashTagModel;
import org.acfse.entities.HashTagEntity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface HashTagMapper {

    HashTagMapper INSTANCE = Mappers.getMapper(HashTagMapper.class);
    HashTagEntity hashTagDtoToHashTagEntity(HashTagDto hashTagDto);
    List<HashTagModel> listHashTagEntityToListHashTagModel(List<HashTagEntity> hashTagEntities);
    HashTagModel hashTagEntityToHashTagModel(HashTagEntity hashTagEntity);

    HashTagEntity hashTagModelToHashTagEntity(HashTagModel hashTagModel);
}
