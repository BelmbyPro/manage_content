package org.acfse.mappers;

import org.acfse.data.UserDto;
import org.acfse.data.models.UserModel;
import org.acfse.entities.UserEntity;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    UserModel userEntityToUserModel(UserEntity userEntity);
    UserEntity userDtoToUserEntity(UserDto userDto);
    List<UserModel> userEntityListToUserModelList(List<UserEntity> userEntities);
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void updateUserFromDtoUserModel(UserModel userResponse, @MappingTarget UserEntity userEntity);

    UserEntity userModelToUserEntity(UserModel userModel);
}
