package org.acfse.adapters;

import org.acfse.data.HashTagDto;
import org.acfse.data.models.HashTagModel;
import org.acfse.entities.HashTagEntity;
import org.acfse.mappers.HashTagMapper;
import org.acfse.ports.spi.HashTagPersistencePort;
import org.acfse.repositories.HashTagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class HashTagPersistenceAdapter implements HashTagPersistencePort {
    @Autowired
    private HashTagRepository hashTagRepository;

    @Override
    public void addHashTag(HashTagDto hashTagDto) {
        HashTagEntity hashTagEntity = HashTagMapper.INSTANCE.hashTagDtoToHashTagEntity(hashTagDto);
        hashTagRepository.save(hashTagEntity);
    }

    @Override
    public List<HashTagModel> getAllHashTags() {
        return HashTagMapper.INSTANCE.listHashTagEntityToListHashTagModel(hashTagRepository.findAll());
    }

    @Override
    public HashTagModel getHashTagByName(String name) {

        Optional<HashTagEntity> hashTagEntity = hashTagRepository.findByName(name);

        return hashTagEntity.isPresent() ? HashTagMapper.INSTANCE.hashTagEntityToHashTagModel(hashTagEntity.get()) : null;
    }

    @Override
    public HashTagModel getHashTagById(Long id) {
        Optional<HashTagEntity> hashTagEntity = hashTagRepository.findById(id);
        return hashTagEntity.isPresent() ? HashTagMapper.INSTANCE.hashTagEntityToHashTagModel(hashTagEntity.get()) : null;
    }
}
