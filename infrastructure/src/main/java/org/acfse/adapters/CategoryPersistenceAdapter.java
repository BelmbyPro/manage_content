package org.acfse.adapters;

import org.acfse.data.CategoryDto;
import org.acfse.data.models.CategoryModel;
import org.acfse.entities.CategoryEntity;
import org.acfse.mappers.CategoryMapper;
import org.acfse.ports.spi.CategoryPersistencePort;
import org.acfse.repositories.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CategoryPersistenceAdapter implements CategoryPersistencePort {

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public void addCategory(CategoryDto categoryDto) {
        CategoryEntity categoryEntity = CategoryMapper.INSTANCE.categoryDtoToCategoryEntity(categoryDto);
        categoryRepository.save(categoryEntity);

    }

    @Override
    public List<CategoryModel> getAllCategories() {
        return CategoryMapper.INSTANCE.listCategoryEntityToListCategoryModel(categoryRepository.findAll());
    }

    @Override
    public CategoryModel getCategoryByName(String name) {
        Optional<CategoryEntity> categoryEntity = categoryRepository.findByName(name);
        return categoryEntity.isPresent() ? CategoryMapper.INSTANCE.categoryEntityToCategoryModel(categoryEntity.get()) : null;
    }

    @Override
    public CategoryModel getCategoryById(Long id) {
        Optional<CategoryEntity> categoryEntity = categoryRepository.findById(id);
        return categoryEntity.isPresent() ? CategoryMapper.INSTANCE.categoryEntityToCategoryModel(categoryEntity.get()) : null;
    }
}
