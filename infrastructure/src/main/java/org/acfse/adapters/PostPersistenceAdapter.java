package org.acfse.adapters;

import lombok.AllArgsConstructor;
import org.acfse.data.PostDto;
import org.acfse.data.enums.TypePost;
import org.acfse.data.models.PostModel;
import org.acfse.entities.*;
import org.acfse.mappers.*;
import org.acfse.ports.spi.CategoryPersistencePort;
import org.acfse.ports.spi.HashTagPersistencePort;
import org.acfse.ports.spi.PostPersistencePort;
import org.acfse.ports.spi.UserPersistencePort;
import org.acfse.repositories.ListeHashTagPostRepository;
import org.acfse.repositories.PostRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class PostPersistenceAdapter implements PostPersistencePort {

    PostRepository<PostEntity> postRepository;
    PostRepository<QuestionEntity> questionEntityPostRepository;
    PostRepository<StoryEntity> storyEntityPostRepository;
    UserPersistencePort userPersistencePort;
    CategoryPersistencePort categoryPersistencePort;
    HashTagPersistencePort hashTagPersistencePort;
    ListeHashTagPostRepository listeHashTagPostRepository;
    PostMapper postMapper;


    @Override
    public void addPost(PostDto postDto) {

        UserEntity userEntity = UserMapper.INSTANCE.userModelToUserEntity(userPersistencePort.getUserById(postDto.getUserId()));
        CategoryEntity categoryEntity = CategoryMapper.INSTANCE.categoryModelToCategoryEntity(categoryPersistencePort.getCategoryById(postDto.getCategoryId()));

        if (TypePost.QUESTION.equals(postDto.getType())) {

            QuestionEntity questionEntity = postMapper.postDtoToQuestionEntity(postDto, userEntity, categoryEntity);
            PostEntity postEntity = postRepository.save(questionEntity);
            System.out.println(postEntity.getId());
            createAssociationClassBetweenPostAndHashTag(postDto, postEntity);
        }
        if (TypePost.STORY.equals(postDto.getType())) {

            StoryEntity storyEntity = postMapper.postDtoToStoryEntity(postDto, userEntity, categoryEntity);
            PostEntity postEntity = postRepository.save(storyEntity);
            createAssociationClassBetweenPostAndHashTag(postDto, postEntity);
        }

    }

    @Override
    public List<PostModel> getAllPosts() {
        List<PostEntity> postEntities = postRepository.findAll();

        return postMapper.listPostEntityToListPostModel(postEntities);
    }

    @Override
    public List<PostModel> getAllQuestions() {
        List<QuestionEntity> allQuestionEntities = questionEntityPostRepository.findAllQuestionEntities();
        return postMapper.listQuestionEntityToListPostModel(allQuestionEntities);
    }

    @Override
    public List<PostModel> getAllStories() {
        List<StoryEntity> storyEntities = storyEntityPostRepository.findAllStoryEntities();
        return postMapper.listStoryEntityToListPostModel(storyEntities);
    }


    private void createAssociationClassBetweenPostAndHashTag(PostDto postDto, PostEntity postEntity) {
        if (postDto.getHashTagIds() != null && !postDto.getHashTagIds().isEmpty()) {
            postDto.getHashTagIds().forEach(id -> {
                HashTagEntity hashTagEntity = HashTagMapper.INSTANCE.hashTagModelToHashTagEntity(hashTagPersistencePort.getHashTagById(id));
                ListeHashTagPost listeHashTagPost = ListeHashTagPost.builder()
                        .hashTagEntity(hashTagEntity)
                        .postEntity(postEntity)
                        .build();
                listeHashTagPostRepository.save(listeHashTagPost);
            });
        }
    }
}
