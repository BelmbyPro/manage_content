package org.acfse.adapters;

import org.acfse.data.UserDto;
import org.acfse.data.models.UserModel;
import org.acfse.entities.UserEntity;
import org.acfse.mappers.UserMapper;
import org.acfse.ports.spi.UserPersistencePort;
import org.acfse.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserPersistenceAdapter implements UserPersistencePort {

    @Autowired
    private UserRepository userRepository;

    @Override
    public void addUser(UserDto userDto) {

        UserEntity userEntity = UserMapper.INSTANCE.userDtoToUserEntity(userDto);
        UserEntity userEntitySaved = userRepository.save(userEntity);
    }

    @Override
    public void deleteUserById(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public void updateUser(UserModel userModel) {

        Optional<UserEntity> userEntity = userRepository.findById(userModel.getId());

        if (userEntity.isPresent()){
            UserMapper.INSTANCE.updateUserFromDtoUserModel(userModel, userEntity.get());
            userRepository.save(userEntity.get());
        }
    }

    @Override
    public List<UserModel> getUsers() {
        List<UserEntity> userEntities = userRepository.findAll();

        return UserMapper.INSTANCE.userEntityListToUserModelList(userEntities);
    }

    @Override
    public UserModel getUserById(Long id) {
        Optional<UserEntity> userEntity = userRepository.findById(id);
        return getUserModel(userEntity);
    }

    @Override
    public UserModel getUserByEmail(String email) {
        Optional<UserEntity> userEntity = userRepository.findByEmail(email);
        return getUserModel(userEntity);
    }

    private UserModel getUserModel(Optional<UserEntity> userEntity) {
        return userEntity.isPresent() ? UserMapper.INSTANCE.userEntityToUserModel(userEntity.get()) : null;
    }
}
