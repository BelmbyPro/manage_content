package org.acfse.ports.spi;

import org.acfse.data.UserDto;
import org.acfse.data.models.UserModel;

import java.util.List;

public interface UserPersistencePort {
    void addUser(UserDto userDto);
    void deleteUserById(Long id);
    void updateUser(UserModel userModel);
    List<UserModel> getUsers();
    UserModel getUserById(Long id);

    UserModel getUserByEmail(String email);
}
