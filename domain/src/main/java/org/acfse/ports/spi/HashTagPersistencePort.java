package org.acfse.ports.spi;

import org.acfse.data.HashTagDto;
import org.acfse.data.models.HashTagModel;

import java.util.List;

public interface HashTagPersistencePort {
    void addHashTag(HashTagDto hashTagDto);
    List<HashTagModel> getAllHashTags();
    HashTagModel getHashTagByName(String name);
    HashTagModel getHashTagById(Long id);
}
