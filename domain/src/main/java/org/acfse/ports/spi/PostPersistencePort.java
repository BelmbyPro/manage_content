package org.acfse.ports.spi;

import org.acfse.data.PostDto;
import org.acfse.data.models.PostModel;

import java.util.List;

public interface PostPersistencePort {
    List<PostModel> getAllPosts();
    void addPost(PostDto postDto);
    List<PostModel> getAllQuestions();
    List<PostModel> getAllStories();
}
