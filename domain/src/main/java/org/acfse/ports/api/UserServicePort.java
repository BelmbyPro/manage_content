package org.acfse.ports.api;

import org.acfse.data.UserDto;
import org.acfse.data.models.UserModel;

import java.util.List;

public interface UserServicePort {

    void addUser(UserDto userDto);
    void deleteUserById(Long id);
    void updateUser(UserModel userModel);
    List<UserModel> getUsers();
    UserModel getUserById(Long id);
    UserModel getUserByEmail(String email);
}
