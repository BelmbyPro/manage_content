package org.acfse.ports.api;

import org.acfse.data.CategoryDto;
import org.acfse.data.models.CategoryModel;

import java.util.List;

public interface CategoryServicePort {
    void addCategory(CategoryDto categoryDto);
    List<CategoryModel> getAllCategories();
    CategoryModel getCategoryByName(String name);
    CategoryModel getCategoryById(Long id);
}
