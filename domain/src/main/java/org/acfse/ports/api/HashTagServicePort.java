package org.acfse.ports.api;

import org.acfse.data.HashTagDto;
import org.acfse.data.models.HashTagModel;

import java.util.List;

public interface HashTagServicePort {
    void addHashTag(HashTagDto hashTagDto);
    List<HashTagModel> getAllHashTags();
    HashTagModel getHashTagByName(String name);
    HashTagModel getHashTagById(Long id);

}
