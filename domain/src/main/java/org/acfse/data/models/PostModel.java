package org.acfse.data.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.acfse.data.enums.TypePost;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PostModel {
    private Long id;
    private TypePost type;
    private String title;
    private String content;
    private String filename;
}
