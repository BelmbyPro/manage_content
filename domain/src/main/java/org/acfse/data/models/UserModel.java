package org.acfse.data.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.acfse.data.enums.Genre;

import java.util.Date;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserModel {
    private Long id;
    private String nom;
    private String prenom;
    private String email;
    private Date dateDeNaissance;
    private String descriptionDuProfil;
    private Genre genre;
    private String travail;
    private String ecole;
    private String adresse;
    private String langue;
}
