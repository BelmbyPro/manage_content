package org.acfse.data.enums;

public enum Genre {
    FEMININ("F"), MASCULIN("M");

    private String code;

    Genre(String code) {
        this.code=code;
    }
}
