package org.acfse.data;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.acfse.data.enums.TypePost;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class PostDto {
    @NotNull(message = "type of post is mandatory")
    private TypePost type;
    @NotBlank(message = "title is mandatory")
    private String title;
    @NotBlank(message = "contentORfilename is mandatory")
    private String contentORfilename;
    @NotNull(message = "userId is mandatory")
    private Long userId;
    @NotNull(message = "categoryId is mandatory")
    private Long categoryId;
    private List<Long> hashTagIds;
}
