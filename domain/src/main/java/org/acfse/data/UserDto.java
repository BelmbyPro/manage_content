package org.acfse.data;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.acfse.data.enums.Genre;

import javax.validation.constraints.NotBlank;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserDto {
    @NotBlank(message = "Name is mandatory")
    private String nom;
    private String prenom;
    @NotBlank(message = "Email is mandatory")
    private String email;
    private Date dateDeNaissance;
    private String descriptionDuProfil;
    private Genre genre;
    private String travail;
    private String ecole;
    private String adresse;
    private String langue;
 
}
